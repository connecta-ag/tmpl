<?php

namespace CAG\Tmpl\ViewHelpers;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2018 Connecta AG Dev Team
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use CAG\Tmpl\Provider\PageTitleProvider;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3Fluid\Fluid\Core\Exception;
use TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface;
use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;

class PagetitleViewHelper extends AbstractViewHelper
{

    /**
     * Arguments initialization
     */
    public function initializeArguments()
    {

        // Arguments taken from EXT:vhs's TitleViewHelper
        $this->registerArgument('title', 'string', 'Title tag content');
    }

    /**
     * Set the page title via FLUID viewhelper
     *
     * @param array $arguments
     * @param \Closure $renderChildrenClosure
     * @param RenderingContextInterface $renderingContext
     * @throws Exception
     */
    public static function renderStatic(
        array $arguments,
        \Closure $renderChildrenClosure,
        RenderingContextInterface $renderingContext
    ) {
        if ('BE' === TYPO3_MODE) {
            return;
        }
        if (false === empty($arguments['title'])) {
            $title = $arguments['title'];
        } else {
            $title = $renderChildrenClosure();
        }

        $pageTitleProvider = GeneralUtility::makeInstance(PageTitleProvider::class);
        $pageTitleProvider->setTitle($title);
    }
}
