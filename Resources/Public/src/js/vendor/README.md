# CAG notes on vendor / 3rd party JS
You can basically use this folder for vendor JS. But pulling it eg. via
package.json or composer is to be preferred since it is only needed for
compiling the CSS. 

If vendor JS is to be manipulated, this place and the GIT in general are
the right places to do so, of course.
