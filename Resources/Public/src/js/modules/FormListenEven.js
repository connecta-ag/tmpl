'use strict';

/**
 * Just a basic listener event to form
 *
 */


export default class FormListenEvent {

  constructor() {
    this.whatAmI = `I'm an FormListenEvent`,
      this.selectorForm = 'form',
      this.selectorInput = 'input',
      this.selectorClassInput = '.input',

      this.selectButton = 'button', // selector tag button to disable
      this.selectorDisableLoading = 'button--no-loadingsubmit', // class to disable loading animation
      this.layoutButton = true, // layout to show indicator on button
      this.fullScreen = false, // active overlay full screen
      this.textField = 'Sending ...'; // new text
  }

  // init class
  init() {
    let self = this;

    if (document.querySelectorAll(self.selectorForm).length > 0) {
      self.listenerEventBlur();
      self.listenerEventChange();
      self.listenerEventSubmit();
    }
  }

  // add event listener submit
  listenerEventSubmit() {
    let self = this,
      selectoAllForm = document.querySelectorAll(self.selectorForm),
      selectorButtonallSubmit = document.querySelectorAll('.' + self.selectButton),
      textField = (self.textField == '') ? $DOMButton.value : self.textField;

    // added eventlistener click in each button
    for (let i = 0; i < selectorButtonallSubmit.length; i++) {
      selectorButtonallSubmit[i].addEventListener('click', this.addEventListenerClick, true);
    }

    for (let j = 0; j < selectoAllForm.length; j++) {
      selectoAllForm[j].addEventListener("submit", function (e) {
        e.preventDefault(); // for debugging: stop form from submitting to test
        this.classList.add("form--submit");

        // built new button with loading animate
        if (self.layoutButton) {

          // active animate on button
          let $DOMButton = '';

          // identify the clicked button
          for (let i = 0; i < selectorButtonallSubmit.length; i++) {
            if (selectorButtonallSubmit[i].classList.contains("submited")) {

              if (selectorButtonallSubmit[i].classList.contains(self.selectorDisableLoading)) {
                return true; // disable loading animate
              }

              $DOMButton = selectorButtonallSubmit[i];
            }
          }

          //clone all submits in form
          let childrensButtons = $DOMButton.parentNode.children;
          let createNewButtons = '';

          for (let i = 0; i < childrensButtons.length; i++) {

            if (!childrensButtons[i].classList.contains("submited") && childrensButtons[i].classList.contains("button")) {
              createNewButtons += '<button class="' + childrensButtons[i].className + '">' + childrensButtons[i].value + childrensButtons[i].textContent + '</button>'; // button without icons
              childrensButtons[i].style.display = "none";
            } else if (childrensButtons[i].classList.contains("button")) {
              createNewButtons += '<button class="' + childrensButtons[i].className + ' showLoadingButton">' + textField + '</button>'; // button with loader icon
              childrensButtons[i].style.display = "none";
            } else if (childrensButtons[i].tagName == 'LABEL') {
              createNewButtons += childrensButtons[i].outerHTML;
              childrensButtons[i].style.display = "none";
            }

          }

          // clone submit button with loader
          let elementContent = document.createElement('div');
          elementContent.className = "showLoadingIndicator";
          $DOMButton.parentNode.appendChild(elementContent);
          document.getElementsByClassName("showLoadingIndicator")[0].innerHTML = createNewButtons;
        }
        // built a overlay with loading animate on form
        else {
          // full screen or only on form
          selectoAllForm[j] = (!self.fullScreen) ? selectoAllForm[j] : document.body;

          // active animate on form
          selectoAllForm[j].classList.add("showLoadingIndicator");
          selectoAllForm[j].appendChild(document.createElement("div"));
          selectoAllForm[j].lastChild.innerHTML = '<p class="showLoadingText">' + textField + '</p>';
        }
      });
    }
  }

  // add a class to finger
  addEventListenerClick() {
    this.className += " submited";
  }

  // add event listener blur
  listenerEventBlur() {
    let self = this,
      inputsFocus = document.querySelectorAll(self.selectorClassInput);

    for (let i = 0; i < inputsFocus.length; i++) {
      inputsFocus[i].addEventListener('blur', function () {
        if (this.value.length > 0 && !this.classList.contains('not-empty')) {
          this.className += ' not-empty';
        } else {
          this.className = this.className.replace(' not-empty', '');
        }
      });
    }
  }

  // add event listener change
  listenerEventChange() {
    let self = this,
      searchParentForm = undefined,
      inputsChange = document.querySelectorAll(self.selectorInput);

    for (let i = 0; i < inputsChange.length; i++) {
      inputsChange[i].addEventListener('change', function () {

        searchParentForm = this.parentElement;
        // WARNING: loop infinite 
        for (let j = 0; j < 10; j++) {
          if (searchParentForm.nodeName == 'FORM') {
            if (!searchParentForm.classList.contains('form--changed')) {
              searchParentForm.className += ' form--changed';
            }
            break;
          } else {
            searchParentForm = searchParentForm.parentElement;
          }
        }
      });
    }
  }

}
