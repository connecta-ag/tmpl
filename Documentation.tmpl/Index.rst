﻿.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.

.. include:: Includes.txt

.. _start:

=============================================================
tmpl
=============================================================

.. only:: html

	:Classification:
		tmpl

	:Version:
		|release|

	:Language:
		en

	:Description:
		Provider Extension for templating etc.

	:Keywords:
		comma,separated,list,of,keywords

	:Copyright:
		2017

	:Author:
		Andreas Bücking, Jochen Rieger, Matthias Krams

	:Email:
		a.buecking@connecta.ag, j.rieger@connecta.ag, m.krams@connecta.ag

	:License:
		This document is published under the Open Content License
		available from http://www.opencontent.org/opl.shtml

	:Rendered:
		|today|

	The content of this document is related to TYPO3,
	a GNU/GPL CMS/Framework available from `www.typo3.org <http://www.typo3.org/>`_.

	**Table of Contents**

.. toctree::
	:maxdepth: 3
	:titlesonly:

	Introduction/Index
	User/Index
	Administrator/Index
	Configuration/Index
	Developer/Index
	KnownProblems/Index
	ToDoList/Index
	ChangeLog/Index
	Links
