# CAG Basis Extension

Dies ist die CAG Basis-Extension zur Einbindung in neuen Projekten. Um den vollen Feature-Umfang nutzen können (z.B. automatische Environment-basierte Config), sollte sie in [cag_project](https://bitbucket.org/connecta-ag/cag_project) eingebettet sein.

## Die Features

* Grundlegende Ordnerstrukturen zur Ablage von Klassen, Templates, Assets, Konfigurationen
* Ein Basis-Set an TypoScript und TS Config Optionen
* PHP Klassen und Config basierend auf [cag_project's AdditionalConf](https://bitbucket.org/connecta-ag/cag_project/src/49e384e2ee48110558c7cefd5152911201479881/web/typo3conf/AdditionalConfiguration.php?at=master&fileviewer=file-view-default) für
    + Logging nach [/var/log/typo3-default.log](https://bitbucket.org/connecta-ag/tmpl/src/92ae85c8dd811d8fc476705439b08865ebdb866e/Configuration/Typo3ConfVars/Override.php?at=master)
    + Dynamisches Laden von Config-Optionen (z.B. DB-Credentials) via *.env* - siehe [.env.example](https://bitbucket.org/connecta-ag/cag_project/src/49e384e2ee48110558c7cefd5152911201479881/.env.example?at=master)
* Context-abhängige Debugging- und Caching-Einstellungen - siehe [Typo3ConfVars](https://bitbucket.org/connecta-ag/tmpl/src/4b886c998dfbf8cfcea75c9b4277396548ef84f0/Configuration/Typo3ConfVars/?at=master)
* Optimierte Ordnerstruktur für Assets und deren Source-Dateien - siehe auch [cag_project/package.json](https://bitbucket.org/connecta-ag/cag_project/src/49e384e2ee48110558c7cefd5152911201479881/package.json?at=master&fileviewer=file-view-default)
* Page Title Handling (v9) für Page Titles von Plugin-basierten DetailSeiten

## Einbinden der Basis Extension

**TODO:** überarbeiten und mit cag_project abgleichen!

Zur Einbindung der Extension muss diese einfach in das Projekt eingefügt werden.

> **Note:**

> - git clone git@bitbucket.org:connecta-ag/tmpl.git
> - mv tmpl --your-project-ext-folder--
> - Im TYPO3-Backend die Basis Extension Root TS Template als Include Static einbinden.

## Enthaltene Funktionalitäten

### Frontend Toolchain

Die Frontend Toolchain (via npm Task Runner) ist zu finden in [Projekt-Template](https://bitbucket.org/connecta-ag/cag_project).

### Page Templates

#### Layout
Definition des Standard-Layouts unter: *EXT:tmpl/Resources/Private/Pages/Layouts/Default.html*

#### Standard-Template
Es ist ein einspaltiges standard Fluid-Template enthalten unter: *EXT:Tmpl/Templates/Page/00_Standard.html*

### Partial "Image Responsive"
Zur einheitlichen Ausgabe der Bilder ist das Partial "Image Responsive" enthalten. Es stehen verschiedene Ratios zur Verfügung:

 - figure__16-9
 - figure__4-3
 - figure__wide
 - figure__ultra-wide
 - figure__no-ratio

### Page Title Handling (TYPO3 v9) ###

Siehe https://docs.typo3.org/typo3cms/CoreApiReference/ApiOverview/PageTitleApi/

Code-Beispiel in einer `MyController->showAction()`:

```php
<?php

// ...

use TYPO3\CMS\Core\Utility\GeneralUtility;
use CAG\Tmpl\Provider\PageTitleProvider;

// ...

// set the page title
$pageTitleProvider = GeneralUtility::makeInstance(PageTitleProvider::class);
$pageTitle = $myObject->getTitle();
$pageTitleProvider->setTitle($pageTitle);
```

Auch in FLUID-Templates als ViewHelper zu verwenden:

```html
<html xmlns="http://www.w3.org/1999/xhtml" lang="en"
      xmlns:f="http://typo3.org/ns/TYPO3/Fluid/ViewHelpers"
      xmlns:cag="http://typo3.org/ns/CAG/Tmpl/ViewHelpers">

  <!-- ... -->

  <cag:pagetitle title="hello world"/>

  <!-- ... -->

</html>
```

### Page not found handling

**TODO:** mit neuem Routing in TYPO3 v9 abgleichen und aktualisieren hier in der README.

Im Pfad *EXT:tmpl/Classes/Utility/PageNotFoundHandling.php* liegt die die Klasse zur Erweiterung des 404-Handlings.
Standardmäßig wirft TYPO3 bei Zugriff einer zugriffgeschützten Seite den Status "404 - Seite nicht gefunden". Gewöhnlich möchte man den Benutzer in diesem Fall allerdings zum Loginformular führen.
Folgende Konfiguration müssen hinzugefügt werden:
```php
$GLOBALS['TYPO3_CONF_VARS']['FE']['pageNotFound_handling'] = 'USER_FUNCTION:EXT:tmpl/Classes/Utility/PageNotFoundHandling.php:user_pageNotFound->pageNotFound';

// Custom configuration for multi-language 404 page, see EXT:tmpl/Classes/Utility/PageNotFoundHandling.php
// ID of the page to redirect to if page was not found
$GLOBALS['TYPO3_CONF_VARS']['FE']['pageNotFound_handling_redirectPageID'] = 123;
// ID of the page to redirect to if current page is access protected
$GLOBALS['TYPO3_CONF_VARS']['FE']['pageNotFound_handling_loginPageID'] = 789;
```
